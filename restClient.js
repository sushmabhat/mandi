var HttpsProxyAgent = require('https-proxy-agent');
var request = require('request');
var mongodb = require('mongodb');
var cron = require('node-schedule');

var proxy = 'http://cis-india-pitc-mumbaiz.proxy.corporate.ge.com:80';
//var agent = new HttpsProxyAgent(proxy);
var jsonData, records;
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/mandi';

function mandiAPIRequest(){
	request({
	  uri: "https://data.gov.in/api/datastore/resource.json?resource_id=9ef84268-d588-465a-a308-a864a43d0070&api-key=44b0f1f5512083256ca216caa9afe1a4",
	  method: "GET",  
	  //agent: agent,
	  timeout: 10000,
	  followRedirect: true,
	  maxRedirects: 10
	}, function(error, response, body) {
		console.log("Error" + error);
		console.log("Response: " + response);
		console.log("Body: "+ body);	
		jsonData = JSON.parse(body);
		records = jsonData.records;
		for (i=0;i<records.length;i++){
			strDate = records[i].arrival_date.split("/");
			console.log("Dates are:"+strDate[2]+"-"+strDate[1]+"-"+strDate[0] + "***" + new Date(strDate[2], strDate[1]-1, strDate[0]));
			records[i].arrival_date = new Date(strDate[2], strDate[1]-1, strDate[0]);			
		}
		mongoDBInsert();
	});
}

function mongoDBInsert(){
	// Use connect method to connect to the Server
	MongoClient.connect(url, function (err, db) {
	  if (err) {
		console.log('Unable to connect to the mongoDB server. Error:', err);
	  } else {
		//HURRAY!! We are connected. :)
		console.log('Connection established to', url);

		// Get the documents collection
		var collection = db.collection('prices');

		// Insert some users
		collection.insert(records, function (err, result) {
		  if (err) {
			console.log(err);
		  } else {
			//console.log('Inserted %d documents into the "prices" collection. The documents inserted with "_id" are:', result.result.n, result);
		  }
		  //Close connection
		  db.close();
		});
	  }
	});
}
mandiAPIRequest();

/* This runs at 3:10AM every Friday, Saturday and Sunday. */
var rule2 = new cron.RecurrenceRule();
rule2.dayOfWeek = [0, 1, 2, 3, 4, 5, 6];
rule2.hour = 3;
rule2.minute = 10;

//var rule = new cron.RecurrenceRule();
//rule.second = 30;
cron.scheduleJob(rule2, function(){
    //console.log('This runs everyday.');
	console.log('running the job now');
	mandiAPIRequest();
});