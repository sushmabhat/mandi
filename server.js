var express   = require('express');
var app       = new express();
var fs        = require('fs');
var mongodb   = require('mongodb');
var mongoose  = require('mongoose');  
var Price     = require('./models/mandi');
var isodate   = require("isodate");
var datejs    = require('./public/js/date');
var bodyParser = require('body-parser');
mongoose.connect('mongodb://localhost:27017/mandi');
var db = mongoose.connection;  
db.on('error', console.error.bind(console, 'connection error:')); 

var varietyArray   = [];
var commodityArray = [];
var districtArray  = [];
var priceArray     = [];
var stateArray     = [];
var stArray        = [];
var marketArray    = [];
var newArray       = [];

db.once('open', function callback() {  
    Price.find({'market':'Malout'}, function(err, users){  
        if(err) return console.err(err);  
        console.log(users);  
    }); 
});

//var multer     = require('multer');

/*app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(multer{{dest:'/tmp/'}});*/

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

var port = process.env.PORT || 8084;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

router.use(function(req, res, next) {
	console.log('Received a request');
	
	// Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
	
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    //res.json({ message: 'hooray! welcome to our api!' });   
	res.sendfile('./public/mandi.html'); // load the single view file (angular will handle the page changes on the front-end)
});

	
	router.route('/getStates')
    .get(function(req, res) {
	    console.log("entered states");
		
		Price.find({}, function(err, states) {
            if (err)
                res.send(err);
			recordStr  = JSON.stringify(states);
			records    = JSON.parse(recordStr);
			console.log("District is:"+records[0].state);
			var state;
			var j = 0;
			stateArray = []; //clears the array
			for (i=0;i<records.length;i++){
				state = records[i].state;
				if (stateArray.indexOf(state) == -1){
					stateArray.push(state);
					/*var obj = new Object();
					j = j+1;
					obj.id = j;
					obj.label = state;
					stateArray.push(obj);*/
				}
            }
			//console.log("Array :"+stateArray);
			res.json(stateArray);			
        });
    }); 
	
	router.route('/getDistrictForState/:state')
    .get(function(req, res) {
		console.log('Input is:' + req.params.state);
		
		Price.find({'state':req.params.state}, function(err, prices) {
            if (err)
                res.send(err);
			recordStr  = JSON.stringify(prices);
			records    = JSON.parse(recordStr);
			console.log("District is:"+records[0].district);
			var district;
			districtArray = []; //clears the array
			for (i=0;i<records.length;i++){
				district = records[i].district;
				if (districtArray.indexOf(district) == -1){
					districtArray.push(district);
				}				
            }
			console.log("Array :"+districtArray);
			res.json(districtArray);			
        });
    }); 
	
	router.route('/getMarketForDistrictState/:state/:district')
    .get(function(req, res) {
		console.log('Input is:' + req.params.state);
		console.log('Input is:' + req.params.district);
		
		Price.find({'state':req.params.state, 'district':req.params.district}, function(err, prices) {
            if (err)
                res.send(err);
			recordStr  = JSON.stringify(prices);
			records    = JSON.parse(recordStr);
			console.log("District is:"+records[0].district + " State is:"+records[0].state);
			marketArray = []; //clears the array
			for (i=0;i<records.length;i++){
				market = records[i].market;
				if (marketArray.indexOf(market) == -1){
					marketArray.push(market);
				}				
            }
			console.log("Array :"+marketArray);
			res.json(marketArray);			
        });
    }); 
	
	router.route('/getCommodityForMarket/:state/:district/:market')
    .get(function(req, res) {
		console.log('Input is:' + req.params.market);
		console.log('DB is ready: ' + mongoose.connection.readyState);
		
        Price.find({'market':req.params.market, 'state':req.params.state, 'district':req.params.district, 'market':req.params.market}, function(err, prices) {
            if (err)
                res.send(err);
			recordStr  = JSON.stringify(prices);
			records    = JSON.parse(recordStr);
			console.log("commodity is:"+records[0].commodity);
			
			commodityArray = []; //clears the array
			for (i=0;i<records.length;i++){
				commodityArray.push(records[i].commodity);
            }
			console.log("Array :"+commodityArray);
			res.json(commodityArray);			
        });
    }); 
	
	router.route('/getVarietyForCommodity/:state/:district/:market/:commodity')
    .get(function(req, res) {
		console.log('Input is:' + req.params.commodity);
		Price.find({'commodity':req.params.commodity, 'state':req.params.state, 'district':req.params.district, 'market':req.params.market, 'commodity':req.params.commodity}, function(err, prices) {
            if (err)
                res.send(err);
			recordStr  = JSON.stringify(prices);
			console.log("fetched varieteis" + recordStr);
			records    = JSON.parse(recordStr);
			
			varietyArray = []; //clears the array
			for (i=0;i<records.length;i++){
				console.log("Variety is:"+records[i].variety);
				varietyArray.push(records[i].variety);
            }
			console.log("Array :"+varietyArray);
			res.json(varietyArray);			
        });
    }); 
	
	router.route('/getDuration/:duration/:state/:district/:market/:commodity/:variety')
    .get(function(req, res) {
		var duration = req.params.duration;
		var now = new Date();
		if (duration == '1'){
				now.setDate(now.getDate() - 7);
				console.log("Week before date 1:" + now);
		}else if (duration == '2'){
				now.setMonth(now.getMonth() - 6);
				console.log("Week before date 2:" + now);
		}	
		Price.find({arrival_date:{$gte: isodate(now)}, state:req.params.state, district:req.params.district, market:req.params.market, commodity:req.params.commodity, variety:req.params.variety}, function(err, prices) {
					if (err)
						res.send(err);
					recordStr  = JSON.stringify(prices);
					records    = JSON.parse(recordStr);
					console.log("records is:"+records);
					
					var day;
					
					for (i=0;i<records.length;i++){
						//console.log("Day is "+ new Date(records[i].arrival_date).getDay());
						day = parseInt(Date.getDaysInMonth(new Date(records[i].arrival_date).getYear(), new Date(records[i].arrival_date).getMonth()));
											
						var obj = new Object();
						obj.day = day;
						obj.price = parseInt(records[i].modal_price);
						//var jsonString= JSON.stringify(obj);
						priceArray.push(obj);
						console.log("Price array:"+priceArray + " length "+ priceArray.length+" price:"+ priceArray[0].price);
					}
					console.log("After Price array:"+priceArray + " length "+ priceArray.length+" price:"+ priceArray[0].price);
					if (duration==2){
						j = 1;
						var jsonString;
						for(k=0;k<priceArray.length;)
						{
							console.log("price array inside:"+k+" " + priceArray[k]);
							pr = priceArray[k].price;
							d  = priceArray[k].day;
							
							console.log("d:"+d+" pr:"+pr);
							//populate the previous values with the first value we find in priceArray
							for(;j<=d;j++){
								console.log("inside for loop");
								var obj = new Object();
								obj.day = j;
								pr = pr +20;
								obj.price = pr+20;
								
								console.log("j:"+j+" pr:"+ pr);
								newArray.push(obj);
							}
							k++;							
						}
					}
					console.log("Array :"+newArray);
					res.json(newArray);			
				});
    }); 


// more routes for our API will happen here
	//router.route('*')
	//.get(function(req, res) {
    //    res.sendFile('./public/Project.html'); // load the single view file (angular will handle the page changes on the front-end)
    //});
// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);


	  
	
/*app.use(express.static('public'));

app.get('/', function(req, res){
	res.sendFile(__dirname + "/" + "index.htm");
});

app.get('/process_get', function(req, res){
	response = {
		first_name: req.query.first_name,
		last_name:  req.query.last_name
	};
	console. log(response);
	res.end(JSON.stringify(response));
});

var server = app.listen(8084, function(){
	var host = server.address().host;
	var port = server.address().port;

	console.log('App is listening on the host %s and port %s', host, port);

});*/

