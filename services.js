var stateService = angular.module('stateService', [])
.service('State', ['$http', function($http) {
	console.log('enetred state service');
	  this.get = function() {
		return $http.get('http://localhost:8084/api/getStates')
		  .then(
			function(response) {
			  console.log("data retruened:"+ response.data);
			  return response.data;
			}
		  );
	  };
	}]);
	
var districtService = angular.module('districtService', [])
.service('District', ['$http', function($http) {
	  this.get = function(state) {
		return $http.get('http://localhost:8084/api/getDistrictForState/' + state)
		  .then(
			function(response) {
			  return response.data;
			}
		  );
	  };
	}]);

var marketService = angular.module('marketService', [])	
.service('Market', ['$http', function($http) {
	  this.get = function(state, district) {
		return $http.get('http://localhost:8084/api/getMarketForDistrictState/' + state + '/' + district + '/')
		  .then(
			function(response) {
			  return response.data;
			}
		  );
	  };
	}]);

var commoditiesService = angular.module('commoditiesService', [])	
.service('Commodity', ['$http', function($http) {
	  this.get = function(state, district, market) {
		return $http.get('http://localhost:8084/api/getCommodityForMarket/' + state + '/' + district + '/' + market)
		  .then(
			function(response) {
			  return response.data;
			}
		  );
	  };
	}]);	

var varietiesService = angular.module('varitiesService', [])	
.service('Variety', ['$http', function($http) {
	  this.get = function(state, district, market, commodity) {
		return $http.get('http://localhost:8084/api/getVarietyForCommodity/' + state + '/' + district + '/' + market + '/' + commodity)
		  .then(
			function(response) {
			  return response.data;
			}
		  );
	  };
	}]);	

var durationService = angular.module('durationService', [])	
.service('Duration', ['$http', function($http) {
	  this.get = function(duration, state, district, market, commodity, variety) {
		return $http.get('http://localhost:8084/api/getDuration/' + duration + '/' + state + '/' + district + '/' + market + '/' + commodity + '/' + variety)
		  .then(
			function(response) {
			  return response.data;
			}
		  );
	  };
	}]);